import torch
from torch.nn.functional import one_hot


class Dataset(torch.utils.data.Dataset):
    """
    Classe pour représenter un jeu de données pour un réseau de neurones
    """

    def __init__(self, data, label, size_vocab, size_label):
        """
        Initialise le jeu de données
        
        Arguments :
            data (list) : Liste de données de longueur N
            label (list) : Liste de labels de longueur N
            size_vocab (int) : Taille du vocabulaire des données
            size_label (int) : Taille du vocabulaire des labels
        """
        self.size_vocab = size_vocab
        self.size_label = size_label
        self.data = data
        self.label = label

    def __len__(self):
        """
        Renvoie la longueur du jeu de données
        """
        return len(self.data)

    def __getitem__(self, index):
        """
        Renvoie les données et les labels à l'index spécifié
        
        Arguments :
            index (int) : L'index des données et des labels à renvoyer
            
        Retourne :
            tuple : Un tuple contenant les données et les labels à l'index spécifié
        """
        return one_hot(self.data[index], num_classes=len(self.size_vocab)), \
            one_hot(self.label[index], num_classes=len(self.size_label))

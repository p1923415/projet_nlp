import torch
from torch.nn.functional import one_hot
from torchtext.vocab import build_vocab_from_iterator

from sklearn.feature_extraction.text import TfidfVectorizer

import nltk
from nltk.corpus import stopwords

import matplotlib.pyplot as plt

from dataset import Dataset
from rnn import RNN

BATCH_SIZE = 100
MAX_WORDS = 10
NB_EPOCHS = 20
HIDDEN_SIZE = 128
OUTPUT_SIZE = 6
EMBEDDING_SIZE = 128

# Télécharge les stopwords
nltk.download('stopwords')


def load_dataset(file):
    """
    Charge les données à partir d'un fichier.

    Arguments :
        file : le nom du fichier contenant les données

    Retourne :
        une liste contenant les données
        une liste contenant les émotions associées aux données
    """
    data = []
    emotions = []
    # Ouvre le fichier
    file = open('./data/' + file)

    for f in file.readlines():
        # Sépare les données et les émotions
        data_f, emotions_f = f.split(sep=';')
        data.append(data_f)
        emotions.append(emotions_f)

    return data, emotions


def yield_tokens(dataset):
    """
    Génère des tokens à partir des données.

    Arguments :
        dataset : la liste des données à tokenizer

    Retourne :
        un token à chaque itération
    """
    for line in dataset:
        # Supprime les caractères spéciaux
        yield line.strip().split()


def remove_stop_words(phrases):
    """
    Supprime les mots vides d'une liste de phrases.

    Arguments :
        phrases : la liste de phrases à traiter

    Retourne :
        une liste de phrases sans les stopwords
    """
    stop_words = set(stopwords.words('english'))
    no_stop_words_sentences = []

    for sentence in phrases:
        no_stop_words_sentence = [word for word in sentence.split() if word not in stop_words]
        no_stop_words_sentences.append(' '.join(no_stop_words_sentence))

    return no_stop_words_sentences


def filter_rare_words(sentences):
    """Filtre les mots rares d'une liste de phrases en utilisant l'IDF (Inverse Document Frequency).
    
    Arguments :
        sentences: Liste de phrases (chaque phrase est représentée sous forme de chaîne de caractères)
    
    Retourne :
        filtered_sentences : Liste de phrases filtrées (chaque phrase est représentée sous forme de chaîne de caractères)
    """
    # Création d'un objet vectorizer pour calculer les scores IDF des mots
    vectorizer = TfidfVectorizer()
    vectorizer.fit_transform(sentences)

    # Récupération des mots rares en comparant leur score IDF à 1
    rare_words = [word for word, score in enumerate(vectorizer.idf_) if score > 1]

    # Filtrage des phrases en enlevant les mots rares
    filtered_sentences = []
    for sentence in sentences:
        filtered_words = [word for word in sentence.split() if word not in rare_words]
        filtered_sentence = " ".join(filtered_words)
        filtered_sentences.append(filtered_sentence)

    # Renvoi de la liste de phrases filtrées
    return filtered_sentences


def train_model(model, dataloader, optimizer, loss_fn, accuracy, count, val=True):
    """
    Entraîne le modèle en utilisant les données fournies dans le dataloader.

    Arguments :
        model : Le modèle à entraîner.
        dataloader : Le DataLoader contenant les données d'entraînement.
        optimizer : L'optimiseur à utiliser pour mettre à jour les poids du modèle.
        loss_fn : La fonction de perte à utiliser pour évaluer la performance du modèle.
        accuracy : Le nombre d'exemples pour lesquels le modèle a prédit la bonne étiquette.
        count : Le nombre total d'exemples dans les données d'entraînement.
        val (optionnel): Si True, le modèle ne sera pas mis à jour (valeur par défaut: True).

    Retourne:
        Un tuple contenant la précision moyenne du modèle sur les données d'entraînement et la perte finale.
    """
    for _, (data, label) in enumerate(dataloader):
        # Initialise les états cachés pour un lot de données
        hidden = model.init_hidden(BATCH_SIZE)

        for j in range(MAX_WORDS):
            # Prépare les données pour l'entrée du modèle en sélectionnant la j-ème colonne de chaque exemple de
            # données du lot
            new_data = torch.FloatTensor(data[:, j, :].float())

            # Applique le modèle aux données et met à jour les états cachés
            output, hidden = model(new_data, hidden)

        # Prépare les labels pour l'entrée du modèle en supprimant la dimension 1 inutile
        label = torch.FloatTensor(label.float()).squeeze(1)

        # Calcule la perte
        loss = loss_fn(output, label)

        if not val:
            # Réinitialise les gradients
            optimizer.zero_grad()

            # Calcule les gradients
            loss.backward()

            # Met à jour les paramètres du modèle
            optimizer.step()

        # Mettre à jour le nombre d'échantillons testés
        count += label.size(0)

        # Calculer la précision
        accuracy += (output.argmax(1) == label.argmax(1)).sum().item()

    return (accuracy / count), loss


def train(model, train_loader, val_loader, optimizer, loss_fn):
    """
    Entraîne le modèle en utilisant les données de l'entraînement spécifiées dans le train_loader et val_loader,
    en utilisant l'optimiseur et la fonction de perte spécifiés.

    Arguments :
        model : le modèle à entraîner
        train_loader : un objet DataLoader contenant les données d'entraînement
        val_loader : un objet DataLoader contenant les données de validation
        optimizer : l'optimiseur à utiliser pour l'entraînement
        loss_fn : la fonction de perte à utiliser pour l'entraînement

    Retourne :
        None
    """
    # Initialiser les variables pour calculer la précision
    accuracy_train = 0
    accuracy_val = 0
    count_train = 0
    count_val = 0

    # Initialiser les variables pour la représentation graphique
    total_accuracy_train = []
    total_accuracy_val = []
    total_loss_train = []
    total_loss_val = []

    print("Training model...")
    for epoch in range(NB_EPOCHS):

        # Entraînement du modèle via les dataloaders d'entraînement et de validation
        accuracy_train, loss_train = train_model(model, train_loader, optimizer, loss_fn, accuracy_train, count_train, val=False)
        accuracy_val, loss_val = train_model(model, val_loader, optimizer, loss_fn, accuracy_val, count_val, val=True)

        # Calcule la loss sur les données d'entraînement et de validation
        loss_train_value = loss_train.item()
        loss_val_value = loss_val.item()

        total_accuracy_train.append(accuracy_train)
        total_accuracy_val.append(accuracy_val)

        total_loss_train.append(loss_train_value)
        total_loss_val.append(loss_val_value)

        print("Epoch: {}, Train Loss: {}, Train Accuracy : {}".format(epoch + 1, loss_train_value, accuracy_train))
        print("Epoch: {}, Validation Loss: {}, Validation Accuracy : {}".format(epoch + 1, loss_val_value, accuracy_val))

        # Si la loss de validation est inférieure à la loss de l'entraînement, on stoppe l'entraînement
        if epoch > 3:
            if total_loss_train > total_loss_val:
                break

    # Sauvegarde le modèle
    torch.save(model.state_dict(), 'model.pth')
    print("Model saved")

    # Affiche la représentation graphique de la précision et de la perte
    plt.plot(total_accuracy_train, label='Accuracy train')
    plt.plot(total_accuracy_val, label='Accuracy val')
    plt.plot(total_loss_train, label='Loss train')
    plt.plot(total_loss_val, label='Loss val')
    plt.title('Accuracy and loss during training')
    plt.show()


def test(model, test_loader):
    """
    Cette fonction teste le modèle en utilisant les données de test.

    Arguments :
        model : Le modèle à tester.
        test_loader : Le DataLoader contenant les données de test ou validation.
        vocab_data : Le vocabulaire pour les données d'entrée.
        vocab_label : Le vocabulaire pour les étiquettes.

    Retourne :
        accuracy : La précision du modèle sur les données de test.
    """
    # Charger les poids du modèle depuis un fichier
    model.load_state_dict(torch.load('model.pth'))
    print("Modèle chargé\nTest du modèle...")

    # Initialiser les variables pour calculer la précision
    accuracy = 0
    count = 0

    # Initialiser les variables pour la représentation graphique
    total_accuracy = []

    # Itérer sur les données de test
    for _, (data, label) in enumerate(test_loader):
        # Initialiser l'état caché du modèle pour chaque lot de données
        hidden = model.init_hidden(BATCH_SIZE)

        # Itérer sur chaque mot de chaque échantillon de données
        for j in range(MAX_WORDS):
            # Convertir les données en tenseur PyTorch
            new_data = torch.FloatTensor(data[:, j, :].float())
            # Obtenir la sortie du modèle en utilisant les données et les états cachés
            output, hidden = model(new_data, hidden)

        # Convertir les étiquettes en tenseur PyTorch
        label = torch.FloatTensor(label.float()).squeeze(1)
        # Calculer la précision
        accuracy += (output.argmax(1) == label.argmax(1)).sum().item()
        # Mettre à jour le nombre d'échantillons testés
        count += label.size(0)
        acc = accuracy / count
        total_accuracy.append(acc)
        print("Accuracy : ", acc)

    # Affiche la représentation graphique de la précision et de la perte
    plt.plot(total_accuracy, label='Accuracy')
    plt.title('Accuracy on test set')
    plt.show()


def main():
    """Fonction principale du programme.
    
    Cette fonction charge les données d'entraînement, de test et de validation,
    pré-traite les données en enlevant les stopwords et les mots rares,
    construit le vocabulaire à partir des données,
    convertit les données en tenseurs,
    crée les datasets d'entraînement, de test et de validation,
    et enfin, construit et entraîne le modèle de réseau de neurones récurrents (RNN).
    """

    print("Chargement du jeu de données...")
    data_train, label_train = load_dataset("train.txt")
    data_test, label_test = load_dataset("test.txt")
    data_val, label_val = load_dataset("val.txt")

    # Concaténation des données d'entraînement, de test et de validation
    data_all = data_train + data_test + data_val
    label_all = label_train + label_test + label_val
    # print("Avant ", data_all[0])

    # Suppression des mots vides des données d'entraînement, de test et de validation
    data_all = remove_stop_words(data_all)
    data_train = remove_stop_words(data_train)
    data_test = remove_stop_words(data_test)
    data_val = remove_stop_words(data_val)
    # print("Après ", data_all[0])

    # Suppression des mots rares des données d'entraînement, de test et de validation
    data_all = filter_rare_words(data_all)
    data_train = filter_rare_words(data_train)
    data_test = filter_rare_words(data_test)
    data_val = filter_rare_words(data_val)
    # print("Après après", data_all[0])

    # Création du vocabulaire à partir des données d'entraînement, de test et de validation
    vocab_data = build_vocab_from_iterator(yield_tokens(data_all), specials=['<unk>'])
    vocab_data.set_default_index(vocab_data['<unk>'])

    # Création du vocabulaire à partir des étiquettes d'entraînement, de test et de validation
    vocab_label = build_vocab_from_iterator(yield_tokens(label_all))

    tokens = []
    for line in data_all:
        tokens.append([])
        # Calculer le nombre de mots à ajouter
        nb_words = MAX_WORDS - len(line.split())
        for word in line.split()[0:MAX_WORDS] + ['<unk>'] * nb_words:
            # Ajouter l'index du mot dans le vocabulaire
            tokens[-1].append(vocab_data[word])

    tokens_label = []
    for line in label_all:
        tokens_label.append([])
        for word in line.split():
            # Ajouter l'index du mot dans le vocabulaire
            tokens_label[-1].append(vocab_label[word])

    # Convertir les données en tenseurs PyTorch
    tokens = torch.tensor(tokens)
    tokens_label = torch.tensor(tokens_label)

    # Création des datasets d'entraînement, de test et de validation
    train_dataset = Dataset(tokens[0:len(data_train)],
                            tokens_label[0:len(label_train)],
                            vocab_data, vocab_label)
    test_dataset = Dataset(tokens[len(data_train):len(data_train) + len(data_test)],
                           tokens_label[len(label_train):len(label_train) + len(label_test)],
                           vocab_data, vocab_label)
    val_dataset = Dataset(tokens[0:len(data_val)],
                          tokens_label[0:len(label_val)],
                          vocab_data, vocab_label)

    # Création des dataloaders d'entraînement, de test et de validation
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=BATCH_SIZE, shuffle=True)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=BATCH_SIZE, shuffle=True)
    val_loader = torch.utils.data.DataLoader(val_dataset, batch_size=BATCH_SIZE, shuffle=True)

    print("Building model...")
    input_size = len(vocab_data)

    # Création du modèle
    model = RNN(input_size, HIDDEN_SIZE, OUTPUT_SIZE, EMBEDDING_SIZE)
    model.float()

    # Apprentissage du modèle
    # train(model, train_loader, val_loader, model.optimizer, model.loss)  # TODO: à décommenter pour l'apprentissage du modèle

    # Test du modèle sur les données de test
    test(model, test_loader)  # TODO: à décommenter pour tester le modèle


if __name__ == '__main__':
    main()

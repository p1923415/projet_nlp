# Projet NLP

## Description
Ce projet est un projet de NLP (Natural Language Processing) qui permet de faire de la classification d'émotions à
partir d'un texte donné. Ce projet a été réalisé sous Python 3.10.

## Installation
Afin de pouvoir utiliser ce projet, il faut installer certaines librairies python. Pour cela, lancez la commande 
suivante à la racine du projet :
```bash
pip install -r requirements.txt
```

## Utilisation
Pour exécuter le projet, il faut lancer le fichier main.py à la racine du projet. Pour cela, lancez la commande suivante :
```bash
python main.py
```
Veuillez noter que si vous souhaitez tester la phase d'apprentissage il faudra décommenter la ligne correspondante dans 
le fichier main.py, il en va de même pour la phase de test. Les lignes à décommenter sont annotées d'un TODO.
Afin d'obtenir de meilleures performances en termes de temps d'exécution, il est possible de commenter toutes les lignes
de traitement des données correspondantes à la phase n'allant pas être utilisée.

## Auteurs
IDIR Nils, JAVEY Paul

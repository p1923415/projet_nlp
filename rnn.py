import torch
from torch import nn


class RNN(nn.Module):
    """Une classe qui représente un réseau de neurones récurrent (RNN) 
    pour prédire une sortie à partir d'une série d'entrées.
    
    Attributs :
        input_size (int) : La taille de l'entrée.
        hidden_size (int) : La taille cachée (ou l'état caché) du RNN.
        output_size (int) : La taille de sortie du RNN.
        embedding_size (int) : La taille de l'espace de représentation des entrées.
        i2h (nn.Linear): Un modèle de couche linéaire qui calcule les poids pour les données d'entrée et d'état caché pour la couche cachée.
        i2o (nn.Linear): Un modèle de couche linéaire qui calcule les poids pour les données d'entrée et d'état caché pour la couche de sortie.
        i2e (nn.Linear): Un modèle de couche linéaire qui calcule les poids pour les données d'entrée pour l'espace de représentation.
        optimizer (torch.optim.Adam): L'optimiseur utilisé pour mettre à jour les poids du modèle.
        loss (nn.CrossEntropyLoss): La fonction de perte utilisée pour évaluer la performance du modèle.
    """

    def __init__(self, input_size, hidden_size, output_size, embedding_size):
        super(RNN, self).__init__()

        self.hidden_size = hidden_size

        self.i2h = nn.Linear(embedding_size + hidden_size, hidden_size, dtype=float)
        self.i2o = nn.Linear(embedding_size + hidden_size, output_size, dtype=float)
        self.i2e = nn.Linear(input_size, embedding_size, dtype=float)
        self.optimizer = torch.optim.Adam(self.parameters(), lr=0.001)
        self.loss = nn.CrossEntropyLoss()

    def forward(self, input, hidden):
        """
        Méthode de propagation avant du modèle.

        Arguments :
            input : un tenseur contenant les données d'entrée du modèle
            hidden : un tenseur contenant l'état caché courant du modèle

        Retourne :
            un tenseur contenant la sortie du modèle
            un tenseur contenant l'état caché mis à jour du modèle
        """
        input = self.i2e(input)
        combined = torch.cat((input, hidden), 1)
        hidden = self.i2h(combined)
        output = self.i2o(combined)

        return output, hidden

    def init_hidden(self, n):
        """
        Méthode pour initialiser l'état caché du modèle.

        Arguments :
            n : le nombre d'échantillons dans les données d'entrée

        Retourne :
            un tenseur contenant l'état caché initialisé
        """
        return torch.zeros(n, self.hidden_size)
